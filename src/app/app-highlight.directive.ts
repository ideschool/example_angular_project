import {AfterViewInit, Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class AppHighlightDirective implements AfterViewInit {
  element: HTMLElement;

  @Input('appHighlight')
  color = '#feff06';

  @Input()
  appHighlightFont = false;

  constructor(ref: ElementRef) {
    this.element = ref.nativeElement;
  }

  ngAfterViewInit(): void {
    this.element.style.backgroundColor = '#feff06';
  }

  @HostListener('mouseover')
  onMouseOver() {
    this.element.style.backgroundColor = this.color;
    if (this.appHighlightFont) {
      this.element.style.color = '#fff';
    }
  }

  @HostListener('mouseout')
  onMouseOut() {
    this.element.style.backgroundColor = '#feff06';
  }
}
