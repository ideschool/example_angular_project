import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AboutUsComponent} from './about-us/about-us.component';
import {AdminPageComponent} from './pages/admin-page/admin-page.component';
import {MainComponent} from './pages/main/main.component';
import {UsersPageComponent} from './pages/users-page/users-page.component';
import {AuthGuardService} from './services/auth-guard.service';
import {LoginPaneComponent} from './pages/login-pane/login-pane.component';

const routes: Routes = [
  {
    component: MainComponent,
    path: '',
  },
  {
    component: AboutUsComponent,
    path: 'about-us',
  },
  {
    component: AdminPageComponent,
    path: 'admin',
    canActivate: [AuthGuardService],
  },
  {
    component: UsersPageComponent,
    path: 'users',
    canActivate: [AuthGuardService],
  },
  {
    component: LoginPaneComponent,
    path: 'login'
  },
  {
    path: '**',
    redirectTo: '',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
