import { Injectable } from '@angular/core';
import {environment as env} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {User} from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  authenticatedUser: User;

  users: User[];

  constructor(private http: HttpClient) {
    this.users = [
      { name: 'john', password: 'smith', role: 'admin'},
      { name: 'adam', password: 'jakis', role: 'user'},
    ];
  }

  get isAuthenticated(): boolean {
    return this.authenticatedUser !== undefined;
  }

  get isAdmin(): boolean {
    if (this.isAuthenticated) {
      return this.authenticatedUser.role === 'admin';
    }
    return false;
  }

  singIn(user: User): Promise<boolean> {
    const idx = this.users.findIndex((dbuser: User) => dbuser.name === user.name);
    if (idx !== -1) {
      this.authenticatedUser = this.users[idx];
      return Promise.resolve(true);
    }
    return Promise.reject('Nieprawidłowe dane logowania');
  }

  signOut() {
    this.authenticatedUser = undefined;
  }
}
