import { Injectable } from '@angular/core';
import {Product} from '../interfaces/product';
import {Observable, Subject} from 'rxjs';
import {ApiService} from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private _productsData: Product[] = [];

  private _products$: Subject<Product[]>;

  constructor(private api: ApiService) {
    this._products$ = new Subject<Product[]>();
    this._products$.next([]);
    this._init();
  }

  private _init(): void {
    this.api.getAll().subscribe((data: Product[]) => {
      this._productsData = data;
      this._products$.next(this._productsData);
    });
  }
  /**
   * Getter for Observable with products
   */
  get products$(): Observable<Product[]> {
    return this._products$;
  }

  /**
   * Adds new product to database
   * @param product Product
   */
  addProduct(product: Product): Promise<any> {
    return this.api.newItem(product).then(() => {
      this._productsData.push(product);
      this._products$.next(this._productsData);
    });
  }

  private _findIndex(id: number): number {
    return this._productsData.findIndex((product: Product) => product.id === id);
  }

  /**
   * Returns product item by Id
   * @param id number
   */
  getById(id: number): Product {
    const idx = this._findIndex(id);
    if (idx !== -1) {
      return this._productsData[idx];
    }
    throw new Error('No such product');
  }

  /**
   * Updates item in products table
   * @param id number
   * @param key string
   * @param value any
   */
  update(id: number, key: string, value: any): void {
    const idx = this._findIndex(id);
    if (idx !== -1) {
      this._productsData[idx][key] = value;
      this._products$.next(this._productsData);
    }
    throw new Error('No such product');
  }
}
