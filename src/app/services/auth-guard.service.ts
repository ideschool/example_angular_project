import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {UsersService} from './users.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private userService: UsersService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const condition = this.userService.isAdmin;
    // if (!condition) {
    //   const redirect = route.routeConfig.path;
    //   this.router.navigate(['/login'], {queryParams: {redirect}});
    // }
    return true;
  }
}
