import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Product} from '../interfaces/product';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(private http: HttpClient) { }

  _asProducts(items: any[]): Product[] {
    return items.map((item: any) => item.data as Product);
  }
  getAll(): Observable<Product[]> {
    const path: string = this._getUrl('all');
    return this.http.get<any[]>(path)
      .pipe(map((data: any[]) => {
        console.log(data);
        return this._asProducts(data);
      }));
  }

  getOne(id: number): Observable<Product> {
    const path: string = this._getUrl(id.toString());
    return this.http.get<any>(path)
      .pipe(map((item: any) => item.result.data as Product));
  }

  newItem(product: Product):  Promise<any> {
    const path: string = this._getUrl(`${product.id}/new`);
    return this.http.post<any>(path, product).toPromise();
  }

  _getUrl(path: string) {
    return `${environment.url}/${environment.shopName}/${path}`;
  }
}
