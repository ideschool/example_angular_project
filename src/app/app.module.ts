import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppHighlightDirective} from './app-highlight.directive';
import {AppValidateDirective} from './app-validate.directive';
import {CountLettersPipe} from './pipes/count-letters.pipe';
import {LoginPaneComponent} from './pages/login-pane/login-pane.component';
import {ProductsService} from './services/products.service';
import {AdminPageComponent} from './pages/admin-page/admin-page.component';
import {MainComponent} from './pages/main/main.component';
import {HttpClientModule} from '@angular/common/http';
import {UsersPageComponent} from './pages/users-page/users-page.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutUsComponent,
    AppHighlightDirective,
    AppValidateDirective,
    CountLettersPipe,
    LoginPaneComponent,
    AdminPageComponent,
    MainComponent,
    UsersPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    ProductsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
