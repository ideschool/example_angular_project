import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services/users.service';
import {User} from '../../interfaces/user';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss'],
  providers: [UsersService]
})
export class UsersPageComponent implements OnInit {
  users: User[];
  constructor(private userService: UsersService) { }

  ngOnInit() {
  }

}
