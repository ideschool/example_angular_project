import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UsersService} from '../../services/users.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../interfaces/user';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-login-pane',
  templateUrl: './login-pane.component.html',
  styleUrls: ['./login-pane.component.scss']
})
export class LoginPaneComponent implements OnInit {
  loginForm: FormGroup;
  redirect: string;

  constructor(private userService: UsersService, private fb: FormBuilder, private route: ActivatedRoute, private router: Router) {
    this.loginForm = this.buildLoginForm();
  }

  get redirectPath(): Observable<string> {
    return this.route.queryParamMap
      .pipe(map((pMap: ParamMap) => pMap.get('redirect')));
  }

  ctrl(name: string): AbstractControl {
    return this.loginForm.get(name);
  }

  buildLoginForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  signIn() {
    const user: User = {
      name: this.ctrl('name').value,
      password: this.ctrl('password').value,
    };
    this.userService.singIn(user)
      .then(() => {
        this.redirectPath.subscribe((redirect: string) => {
          this.router.navigate([`/${redirect}`]);
        });
      })
      .catch((err: any) => {
        console.log(err);
      });
  }

  ngOnInit(): void {
  }
}
