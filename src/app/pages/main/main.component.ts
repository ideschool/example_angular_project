import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Product } from '../../interfaces/product';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getAll()
      .subscribe((data: Product[]) => {
        console.log('foo', data);
      });
  }

}
