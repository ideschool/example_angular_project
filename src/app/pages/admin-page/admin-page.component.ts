import {Component, OnInit} from '@angular/core';
import {ProductsService} from '../../services/products.service';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Product} from '../../interfaces/product';
import {Observable} from 'rxjs';
import {ApiService} from '../../services/api.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {
  productForm: FormGroup;

  _id: number;

  constructor(private productsService: ProductsService, private fb: FormBuilder, private api: ApiService) {
    this._createForm();
  }

  get bttnLabel(): string {
    return this._id !== undefined ? 'Aktualizuj produkt' : 'Zapisz produkt';
  }
  get products$(): Observable<Product[]> {
    return this.productsService.products$;
  }

  ngOnInit() {
  }

  private _createForm(): void {
    this.productForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(10)]],
      price: [null, [Validators.nullValidator, Validators.min(100)]],
      count: [null, [Validators.nullValidator, Validators.min(1)]],
    });
  }

  private _createFormCtl(name: string, defaultValue?: any): void {
    const ctr: AbstractControl = new FormControl(defaultValue);
    this.productForm.addControl(name, ctr);
  }

  getControl(name: string): AbstractControl {
    return this.productForm.get(name);
  }

  private _createProduct(): Product {
    const item: any = {};
    ['name', 'price', 'count'].forEach((key: string) => {
      item[key] = this.getControl(key).value;
    });
    item.id = Date.now();
    return item as Product;
  }

  /**
   * Adds new product to products model in service
   */
  addProduct(): void {
    const product = this._createProduct();
    this.api.newItem(product)
      .then(() => {
        this.productForm.reset();
      })
      .catch((err: any) => {
        // TODO: Handle error
      });
  }


  private _setFormData(data: Product) {
    this.getControl('name').setValue(data.name);
    this.getControl('price').setValue(data.price);
    this.getControl('count').setValue(data.count);
    this._id = data.id;
  }

  getProduct(id: number): void {
    const product: Product = this.productsService.getById(id);
    this._setFormData(product);
  }
}
