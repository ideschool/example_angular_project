import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appValidate]'
})
export class AppValidateDirective {
  input: HTMLInputElement;

  constructor(ref: ElementRef) {
    if (ref.nativeElement instanceof HTMLInputElement) {
      this.input = <HTMLInputElement>ref.nativeElement;
    } else {
      throw new Error('It is not HTMLInputElement');
    }
  }

  @HostListener('input', ['$event.target.value'])
  fofofof(value: string) {
    const valid: boolean = /[a]+/i.test(value);
    this.input.style.color = valid ? '#ff0000' : '#000000';
  }
}
